#! /usr/bin/bash

# Make sure we have the prereqs for nix
if  which apt-get; then
	sudoapt-get install curl bzip2 tar
elif which dnf; then
	sudo dnf install curl bzip2 tar
elif which yum; then
	sudo yum install curl bzip2 tar
fi

# Install nix
curl https://nixos.org/nix/install | sh

# Make sure we have nix in our path
. ~/.nix-profile/etc/profile.d/nix.sh

# Install dev tools
nix-env -i emacs
nix-env -i silver-searcher
nix-env -i git
nix-env -i zsh
nix-env -i tmux
nix-env -i stow

# Clone spacemacs
git clone https://github.com/syl20bnr/spacemacs.git ~/.emacs.d

# Setup dotfiles
git clone https://gwbrown@bitbucket.org/gwbrown/dotfiles.git ~/dotfiles
cd ~/dotfiles

echo "Remember to run stow for each directory in dotfiles!"

echo "zsh" >> ~/.bash_profile
