source ~/.zshscripts/antigen.zsh

antigen use oh-my-zsh

antigen bundle git
antigen bundle Tarrasch/zsh-bd
antigen bundle djui/alias-tips
antigen bundle colored-man-pages
antigen bundle zsh-users/zsh-completions
antigen bundle chrissicool/zsh-256color

antigen bundle zsh-users/zsh-syntax-highlighting
#antigen bundle tarruda/zsh-autosuggestions #Didn't feel like this was really worth it

antigen theme ys

antigen apply

# User configuration

# zle-line-init() {
#     zle autosuggest-start
# }
# zle -N zle-line-init

# Enable smarter tab completion
autoload -U compinit
compinit
zstyle ':completion:*' menu select

# Enable correction
setopt correct

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='emacs'
else
  export EDITOR='emacs'
fi

# Example aliases
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias px="ps ax"
alias ec="emacsclient"
alias ecn="emacsclient -n"
alias ecnc="emacsclient -nc"
